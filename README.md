# HCS Tekton Housekeeping

## Important

Newer releases of Tekton have a built-in Pruner that can be enabled from the `TektonConfig` CR:

```yaml
apiVersion: operator.tekton.dev/v1alpha1
kind: TektonConfig
metadata:
  name: config
  annotations:
    argocd.argoproj.io/sync-wave: "-3"
spec:
  pruner:
    keep: 2
    resources:
    - pipelinerun
    - taskrun
    schedule: '*/15 * * * *'
```

This does not give you the ability to set parameters per Pipeline, but it is a
nice built-in method that can be controlled and enforced by cluster
administrators. It also negates the need to set up a CronJob per namespace.

We **highly** recommend you switch to the new native pruning capabilities.

## About

This repo contains a CronJob defintion (`cleanup-tekton.yml`) that
will spawn a job every 15 minutes to clean up old PipelineRuns.

It does this by looping over all Pipelines in the namespace, and then looping
over all PipelineRuns for that pipeline.

By default it will keep 2 succeeded and 1 failed PipelineRun per Pipeline, but
this can be changed by annotating a Pipeline with the following annotations:

```yaml
housekeeping.hcs-company.com/keep_succeeded: 2
housekeeping.hcs-company.com/keep_failed: 1
```

## How-To Deploy

1. Create an image that has both `oc` and `jq` installed.
   The `Dockerfile` in this repo should give you a good starting point.

2. Edit `cleanup-tekton.yml`
   1. Replace `AN.IMAGE.WITH/JQ_AND_OC` with the path to the image you just made.

3. Create the CronJob
   1. `oc create -f cleanup-tekton.yml`
