# Dockerfile for tekton cleanup
FROM ubi8:latest
LABEL maintainer Riemer Palstra <riemer.palstra@hcs-company.com>
USER root
 
RUN curl -sLo /tmp/oc.tar.gz https://mirror.openshift.com/pub/openshift-v4/clients/oc/latest/linux/oc.tar.gz && tar xvzf /tmp/oc.tar.gz -C /usr/local/bin && dnf install -y jq && dnf clean all && rm -rf /var/cache/dnf
